import { Component } from '@angular/core';
import { ToasterConfig, ToasterService } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';

import { MachinesService } from '../../@core/data/machine/machines.service';
import { GALLERY_CONF, GALLERY_IMAGE } from 'ngx-image-gallery';
import { Screenshot } from '../../@core/data/machine/screenshot.model';

@Component({
  selector: 'ngx-screenshots',
  templateUrl: './screenshots.component.html',
  styleUrls: ['./screenshots.component.scss'],
})
export class ScreenshotsComponent {
  toasterConfiguration: ToasterConfig = new ToasterConfig({
    timeout: 3000,
    positionClass: 'toast-top-right',
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 5,
    showCloseButton: true,
  });
  galleryConfiguration: GALLERY_CONF = {
    imageOffset: '0px',
    showDeleteControl: false,
    showImageTitle: true,
    inline: true,
    backdropColor: '#FFF',
    showCloseControl: false,
  };

  screenshots: GALLERY_IMAGE[] = [];
  machineNames: string[] = [];

  constructor(private machineService: MachinesService,
              private toasterService: ToasterService) {
    machineService.loadMachineNames().subscribe((res) => this.machineNames = res);
  }

  fetchScreenshots(machineName: string) {
    this.screenshots = [];
    this.machineService
      .loadScreenshots(machineName)
      .subscribe((parsedScreenshots: Screenshot[]) => {
        if (parsedScreenshots.length === 0)
          this.toasterService.pop('info', `${machineName}`, `There are no screenshots avaiable.`);
        for (const screenshot of parsedScreenshots) {
          this.screenshots = this.screenshots.concat([{
            url: `data:${screenshot.mimeType};base64,${screenshot.data}`,
            altText: screenshot.fileName,
            title: screenshot.fileName,
          }]);
        }
      });
  }
}
