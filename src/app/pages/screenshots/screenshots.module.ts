import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { ScreenshotsComponent } from './screenshots.component';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { ToasterModule } from 'angular2-toaster';

@NgModule({
  imports: [
    ThemeModule,
    NgxImageGalleryModule,
    ToasterModule,
  ],
  declarations: [
    ScreenshotsComponent,
  ],
})
export class ScreenshotsModule {
}
