import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter';
import { ToasterConfig, ToasterService } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';

import { MachineDataSource } from '../../../@core/data/machine/machine-datasource';
import { MachinesService } from '../../../@core/data/machine/machines.service';
import { Machine } from '../../../@core/data/machine/machine.model';
import { MachineEvent } from '../../../@core/data/machine/machine-event';

@Component({
  selector: 'ngx-machines-view',
  templateUrl: './machines-view.component.html',
  styleUrls: ['./machines-view.component.scss'],
})
export class MachinesViewComponent implements OnInit, OnDestroy {
  private navigationStartSubscription: Subscription;
  private toastsSubscription: Subscription;

  public machineDataSource: DataSource<Machine>;
  public displayedColumns: string[] = ['name', 'isActive', 'isLocked', 'jobCluster', 'actions'];
  public toasterConfiguration: ToasterConfig;

  constructor(private machineService: MachinesService,
              private router: Router,
              private toasterService: ToasterService) {
    this.machineDataSource = new MachineDataSource(this.machineService);
    this.toasterConfiguration = new ToasterConfig({
      timeout: 3000,
      positionClass: 'toast-top-right',
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'flyRight',
      limit: 5,
      showCloseButton: true,
    });
  }

  ngOnInit() {
    this.toastsSubscription = this.machineService.toastsSource$
      .subscribe((event: MachineEvent) => {
        this.toasterService.pop(event.type, event.title, event.description);
      });
    this.navigationStartSubscription = this.router.events
      .filter(value => value instanceof NavigationStart)
      .subscribe(this.closeWebSocket.bind(this));
    this.machineService.openConnection();
  }

  ngOnDestroy() {
    this.navigationStartSubscription.unsubscribe();
    this.toastsSubscription.unsubscribe();
  }

  lockMachine(machineName: string) {
    this.machineService.lock(machineName);
  }

  unlockMachine(machineName: string) {
    this.machineService.unlock(machineName)
  }

  terminateMachine(machineName: string) {
    this.machineService.terminateConnection(machineName);
  }

  @HostListener('window:beforeunload')
  closeWebSocket() {
    this.machineService.closeConnection();
  }
}
