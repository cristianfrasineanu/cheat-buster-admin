import { NgModule } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { TrafficComponent } from './traffic/traffic.component';
import { TrafficChartComponent } from './traffic/traffic-chart.component';
import { MachinesViewComponent } from './machines-view/machines-view.component';
import { ToasterModule } from 'angular2-toaster';

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    ToasterModule,
  ],
  declarations: [
    DashboardComponent,
    TrafficComponent,
    TrafficChartComponent,
    MachinesViewComponent,
  ],
})
export class DashboardModule {
}
