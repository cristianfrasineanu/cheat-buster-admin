import { Component } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { LocalDataSource } from 'ng2-smart-table';
import { ToasterConfig, ToasterService } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';

import { Job } from '../../@core/data/jobs/job.model';
import { JobsService } from '../../@core/data/jobs/jobs.service';
import { DatetimeRendererComponent } from './rendering/datetime-renderer/datetime-renderer.component';
import { RulesRendererComponent } from './rendering/rules-renderer/rules-renderer.component';
import { MachinesRendererComponent } from './rendering/machines-renderer/machines-renderer.component';
import * as moment from 'moment';

@Component({
  selector: 'ngx-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss'],
})
export class JobsComponent {
  toasterConfiguration: ToasterConfig = new ToasterConfig({
    timeout: 3000,
    positionClass: 'toast-top-right',
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 5,
    showCloseButton: true,
  });
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'text',
        editable: false,
      },
      cluster: {
        title: 'Cluster',
        type: 'text',
      },

      rules: {
        title: 'Rules',
        type: 'custom',
        editable: false,
        renderComponent: RulesRendererComponent,
        filter: false,
      },
      machines: {
        title: 'Machines',
        type: 'custom',
        editable: false,
        renderComponent: MachinesRendererComponent,
        filter: false,
      },

      startAt: {
        title: 'Starting time',
        type: 'custom',
        editable: false,
        renderComponent: DatetimeRendererComponent,
        filter: false,
      },
      endAt: {
        title: 'Ending time',
        type: 'custom',
        editable: false,
        renderComponent: DatetimeRendererComponent,
        filter: false,
      },

      pollingRate: {
        title: 'Polling rate',
        type: 'text',
      },
      pushRate: {
        title: 'Push rate',
        type: 'text',
      },
    },
  };
  jobsDataSource: LocalDataSource = new LocalDataSource();

  constructor(private jobsService: JobsService,
              private toasterService: ToasterService) {
    jobsService
      .loadJobs()
      .subscribe(
        (jobs: Job[]) => this.jobsDataSource.load(jobs),
        (err: HttpErrorResponse) => console.error(err.error),
      );
  }

  onEditConfirm(event) {
    this.jobsService
      .updateJob(event.newData)
      .subscribe(
        () => {
          event.confirm.resolve();
          this.toasterService.pop(
            'success',
            'Edit success',
            `The job with ID ${event.newData.id} was saved successfully.`,
          );
        },
        (err: HttpErrorResponse) => {
          event.confirm.reject();
          this.toasterService.pop('error', 'Edit error', err.error);
        },
      );
  }

  onDeleteConfirm(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      const jobId = event.data.id;
      this.jobsService
        .deleteJob(jobId)
        .subscribe(
          () => {
            event.confirm.resolve();
            this.toasterService.pop(
              'success',
              'Delete success',
              `The job with ID ${jobId} was deleted successfully.`,
            );
          },
          (err: HttpErrorResponse) => {
            event.confirm.reject();
            this.toasterService.pop('error', 'Delete error', err.error);
          },
        );
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm(event) {
    const newJob = event.newData;
    newJob.startAt = moment(Date.now())
      .format('YYYY-MM-DDTHH:MM:SS');
    newJob.endAt = moment(Date.now())
      .add(3, 'hours')
      .format('YYYY-MM-DDTHH:MM:SS');
    newJob.machines = [];
    newJob.rules = [];

    this.jobsDataSource
      .getAll()
      .then((jobs: Job[]) => {
        if (!newJob.pollingRate) {
          event.confirm.reject();
          this.toasterService.pop('error', 'Create error', 'Please provide a valid polling rate interval!');
          return;
        }
        for (const job of jobs) {
          if (job.cluster === newJob.cluster) {
            event.confirm.reject();
            this.toasterService.pop('error', 'Create error', 'The cluster name must be unique!');
            return;
          }
        }

        this.jobsService
          .createJob(newJob)
          .subscribe(
            (res: any) => {
              newJob.id = res;
              event.confirm.resolve(newJob);
              this.toasterService.pop(
                'success',
                'Create success',
                `The job with ID ${event.newData.id} was created successfully.`,
              );
            },
            (err: HttpErrorResponse) => {
              event.confirm.reject();
              this.toasterService.pop('error', 'Create error', err.error);
            },
          );
      });
  }
}
