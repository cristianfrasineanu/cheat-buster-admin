import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ToasterModule } from 'angular2-toaster';

import { JobsComponent } from './jobs.component';
import { JobsService } from '../../@core/data/jobs/jobs.service';
import { DatetimeRendererComponent } from './rendering/datetime-renderer/datetime-renderer.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { RulesRendererComponent } from './rendering/rules-renderer/rules-renderer.component';
import { RulesModalComponent } from './rendering/rules-renderer/rules-modal/rules-modal.component';
import { MachinesRendererComponent } from './rendering/machines-renderer/machines-renderer.component';
import { MachinesModalComponent } from './rendering/machines-renderer/machines-modal/machines-modal.component';

@NgModule({
  imports: [
    ThemeModule,
    Ng2SmartTableModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ToasterModule,
  ],
  declarations: [
    JobsComponent,
    DatetimeRendererComponent,
    RulesRendererComponent,
    RulesModalComponent,
    MachinesRendererComponent,
    MachinesModalComponent,
  ],
  providers: [
    JobsService,
  ],
  entryComponents: [
    DatetimeRendererComponent,
    RulesRendererComponent,
    RulesModalComponent,
    MachinesRendererComponent,
    MachinesModalComponent,
  ],
})
export class JobsModule {
}
