import { Component } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Job } from '../../../../@core/data/jobs/job.model';
import { RulesModalComponent } from './rules-modal/rules-modal.component';
import { JobsService } from '../../../../@core/data/jobs/jobs.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  templateUrl: './rules-renderer.component.html',
})
export class RulesRendererComponent implements ViewCell {
  value: string | number;
  rowData: Job;

  constructor(private modalService: NgbModal,
              private jobsService: JobsService) {
  }

  showRulesEditor() {
    const rulesModal = this.modalService.open(RulesModalComponent, { size: 'lg', container: 'nb-layout' });
    rulesModal.componentInstance.handleSave = this.onModalSave.bind(this);
    rulesModal.componentInstance.rules = this.rowData.rules;
  }

  private onModalSave(newRules) {
    this.jobsService
      .updateJob(this.rowData)
      .subscribe(
        (res: any) =>
          console.info(`The rules corresponding to job with ID ${this.rowData.id} were saved successfully.`),
        (err: HttpErrorResponse) => console.error(err.error),
      );
  }
}
