import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Rule } from '../../../../../@core/data/jobs/rule.model';
import { LocalDataSource } from 'ng2-smart-table';
import { remove } from 'lodash';

@Component({
  selector: 'ngx-rules-modal',
  templateUrl: './rules-modal.component.html',
})
export class RulesModalComponent implements OnInit {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'text',
        editable: false,
      },
      action: {
        title: 'Action Type',
        type: 'text',
        valuePrepareFunction: (value) => {
          if (+value === 0)
            return 'Web Request';
          else if (+value === 1)
            return 'Protocol Usage';
          else if (+value === 2)
            return 'System Event';
          else if (+value === 3)
            return 'System Process';
        },
        editor: {
          type: 'list',
          config: {
            list: [
              { value: 0, title: 'Web Request' },
              { value: 1, title: 'Protocol Usage' },
              { value: 2, title: 'System Event' },
              { value: 3, title: 'System Process' },
            ],
          },
        },
      },
      type: {
        title: 'Rule Type',
        type: 'text',
        valuePrepareFunction: (value) => {
          if (+value === 0) {
            return 'Whitelist';
          }
          if (+value === 1)
            return 'Blacklist';
        },
        editor: {
          type: 'list',
          config: {
            list: [
              { value: 0, title: 'Whitelist' },
              { value: 1, title: 'Blacklist' },
            ],
          },
        },
      },
      filter: {
        title: 'Filter Value',
        type: 'text',
      },
    },
  };
  rulesDataSource: LocalDataSource = new LocalDataSource();

  rules: Rule[];
  handleSave: (newRules: Rule[]) => void;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
    this.rulesDataSource.load(this.rules);
  }

  saveRules() {
    this.handleSave && this.handleSave(this.rules);
    this.close();
  }

  close() {
    this.activeModal.close();
  }

  onDeleteConfirm(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      remove(this.rules, (rule: Rule) => rule.id === event.data.id);
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event) {
    const sanitizedRule: Rule = {
      id: +event.newData.id,
      filter: event.newData.filter,
      action: +event.newData.action,
      type: +event.newData.type,
    };
    event.confirm.resolve(sanitizedRule);
  }

  onCreateConfirm(event) {
    const newRule: any = {
      filter: event.newData.filter,
      action: +event.newData.action,
      type: +event.newData.type,
    };
    event.confirm.resolve(newRule);
  }
}
