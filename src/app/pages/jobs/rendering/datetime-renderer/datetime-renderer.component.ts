import { Component, Inject, Input, OnInit } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';
import * as moment from 'moment-timezone';

import { JobsService } from '../../../../@core/data/jobs/jobs.service';
import { Job } from '../../../../@core/data/jobs/job.model';
import { TIMEZONE_VALUE } from '../../../../@core/app-config.constants';

@Component({
  templateUrl: './datetime-renderer.component.html',
  styleUrls: ['./datetime-renderer.component.scss'],
})
export class DatetimeRendererComponent implements ViewCell, OnInit {
  private type: string;
  // The startAt and endAt fields are non-editable. Hence, when cancelling a row edit,
  // even though the editing is handled here, the value passed to the component will be the initial one.
  private static editedDates: { id: number, startAt: string[], endAt: string[] }[] = [];
  private static isoFormat: string = 'YYYY-MM-DDTHH:MM:SS';

  currentDate: string;

  @Input() value: string;
  @Input() rowData: Job;

  constructor(private jobsService: JobsService,
              @Inject(TIMEZONE_VALUE) private timezoneValue: string) {
  }

  ngOnInit() {
    // Populate the arrays with the initial date values.
    let editedValues = DatetimeRendererComponent.editedDates.find((value) => value.id === this.rowData.id);
    if (!editedValues) {
      editedValues = { id: this.rowData.id, startAt: [], endAt: [] };
      DatetimeRendererComponent.editedDates.push(editedValues);
    }
    if (!editedValues.startAt.length && this.value === this.rowData.startAt)
      editedValues.startAt.unshift(this.value);
    if (!editedValues.endAt.length && this.value === this.rowData.endAt)
      editedValues.endAt.unshift(this.value);

    this.determineCurrentDate(editedValues);
  }

  private determineCurrentDate(values) {
    if (values.startAt.findIndex((date) => date === this.value) > -1) {
      this.type = 'startAt';
      this.currentDate = moment(values.startAt[0], moment.ISO_8601)
        .utc()
        .format(DatetimeRendererComponent.isoFormat);
      return;
    } else if (values.endAt.findIndex((date) => date === this.value) > -1) {
      this.type = 'endAt';
      this.currentDate = moment(values.endAt[0], moment.ISO_8601)
        .utc()
        .format(DatetimeRendererComponent.isoFormat);
      return;
    } else throw new Error('The DateTime renderer type must be startAt or endAt!');
  }

  onDateTimeChange() {
    this.rowData[this.type] = moment(this.currentDate).format(DatetimeRendererComponent.isoFormat);
    DatetimeRendererComponent.editedDates
      .find((value) => value.id === this.rowData.id)[this.type]
      .unshift(this.rowData[this.type]);

    this.jobsService
      .updateJob(this.rowData)
      .subscribe(
        () => console.info(`Updated ${this.type} successfully for the selected job.`),
        (err: any) => console.error(err.error),
      );
  }
}
