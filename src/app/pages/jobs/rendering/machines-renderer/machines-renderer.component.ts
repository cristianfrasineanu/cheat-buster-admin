import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ViewCell } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { MachinesModalComponent } from './machines-modal/machines-modal.component';
import { MachinesService } from '../../../../@core/data/machine/machines.service';
import { JobsService } from '../../../../@core/data/jobs/jobs.service';
import { Job } from '../../../../@core/data/jobs/job.model';

@Component({
  templateUrl: './machines-renderer.component.html',
})
export class MachinesRendererComponent implements ViewCell {
  value: string | number;
  rowData: Job;

  constructor(private modalService: NgbModal,
              private machinesService: MachinesService,
              private jobsService: JobsService) {
  }

  showMachinesEditor() {
    const machinesModal = this.modalService.open(MachinesModalComponent, { size: 'lg', container: 'nb-layout' });
    machinesModal.componentInstance.handleSave = this.onModalSave.bind(this);

    this.machinesService
      .loadMachineNames()
      .subscribe(
        (machines: string[]) => {
          machinesModal.componentInstance.jobMachines = this.rowData.machines;
          machinesModal.componentInstance.allMachines = machines;
        },
        (err: HttpErrorResponse) => console.error(err.error),
      );
  }

  private onModalSave(newMachines) {
    this.jobsService
      .updateJob(this.rowData)
      .subscribe(
        () =>
          console.info(`The machines corresponding to job with ID ${this.rowData.id} were saved successfully.`),
        (err: HttpErrorResponse) => console.error(err.error),
      );
  }
}

