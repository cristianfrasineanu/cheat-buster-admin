import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalDataSource } from 'ng2-smart-table';
import { remove } from 'lodash';

@Component({
  selector: 'ngx-machines-modal',
  templateUrl: './machines-modal.component.html',
})
export class MachinesModalComponent {
  private _allMachines: string[];

  get allMachines(): string[] {
    return this._allMachines;
  }

  set allMachines(machines: string[]) {
    this._allMachines = machines;
    this.initializeDataSource();
  }

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
  };
  machinesDataSource: LocalDataSource = new LocalDataSource();

  jobMachines: any;
  handleSave: (newMachines: string[]) => void;

  constructor(private activeModal: NgbActiveModal) {
  }

  private initializeDataSource() {
    this.settings = Object.assign({
      actions: {
        edit: false,
      },
      columns: {
        name: {
          title: 'Machine Name',
          type: 'text',
          editor: {
            type: 'completer',
            config: {
              completer: {
                data: this.allMachines,
              },
            },
          },
        },
      },
    }, this.settings);
    this.machinesDataSource.load(this.jobMachines);
  }

  saveMachines() {
    this.handleSave && this.handleSave(this.jobMachines);
    this.close();
  }

  close() {
    this.activeModal.close();
  }

  onDeleteConfirm(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      remove(this.jobMachines, (machine: any) => machine.name === event.data.name);
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm(event) {
    if (!event.newData || event.newData.name === '') {
      event.confirm.reject();
      return;
    }
    for (const machine of this.jobMachines)
      if (machine.name === event.newData.name) {
        event.confirm.reject();
        return;
      }

    event.confirm.resolve();
  }
}
