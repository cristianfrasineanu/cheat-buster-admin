import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: NbAuthService, private router: Router) {
  }

  canActivate(): Observable<boolean> {
    return this.authService
      .isAuthenticated()
      .pipe(tap(authenticated => {
        if (!authenticated) {
          this.router.navigate(['auth', 'login']);
          return;
        }
        this.authService.getToken().subscribe(token => {
          const tokenPayload = (<NbAuthJWTToken> token).getPayload();
          if (!token.isValid()
            || tokenPayload['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'] !== 'Supervisor')
            this.router.navigate(['auth', 'login']);
        });
      }));
  }
}
