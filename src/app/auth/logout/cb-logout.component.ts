import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NB_AUTH_OPTIONS } from '@nebular/auth/auth.options';
import { getDeepFromObject } from '@nebular/auth/helpers';
import { NbAuthService } from '@nebular/auth/services/auth.service';
import { NbTokenService } from '@nebular/auth';

@Component({
  selector: 'ngx-nb-logout',
  templateUrl: './cb-logout.component.html',
})
export class CbLogoutComponent implements OnInit {
  redirectDelay: number = 0;

  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS) protected config = {},
              protected router: Router,
              private tokenService: NbTokenService) {
    this.redirectDelay = this.getConfigValue('forms.logout.redirectDelay');
  }

  ngOnInit(): void {
    this.logout();
  }

  logout(): void {
    this.tokenService
      .clear()
      .subscribe(() => {
        setTimeout(() => {
          return this.router.navigateByUrl('/');
        }, this.redirectDelay);
      });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}
