import { Component } from '@angular/core';

@Component({
  selector: 'ngx-centered-column-layout',
  styleUrls: ['./centered-column.layout.scss'],
  template: `
    <nb-layout center="true">
      <nb-layout-header fixed>
        <ngx-header position="normal"></ngx-header>
      </nb-layout-header>

      <nb-sidebar class="menu-sidebar"
                  tag="menu-sidebar"
                  responsive
                  left="true">
        <nb-sidebar-header>
           <!--TODO: Link to the project repo-->
          <a href="#" class="btn btn-hero-success main-btn">
            <i class="ion ion-social-github"></i> <span>Contribute</span>
          </a>
        </nb-sidebar-header>
        <ng-content select="nb-menu"></ng-content>
      </nb-sidebar>

      <nb-layout-column class="main-content">
        <ng-content select="router-outlet"></ng-content>
      </nb-layout-column>

      <nb-layout-footer fixed>
        <ngx-footer></ngx-footer>
      </nb-layout-footer>
    </nb-layout>
  `,
})
export class CenteredColumnLayoutComponent {
}
