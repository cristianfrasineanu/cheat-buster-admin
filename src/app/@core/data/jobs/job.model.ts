import { Rule } from './rule.model';
import { Machine } from '../machine/machine.model';

export interface Job {
  id: number,
  cluster: string,
  rules: Rule[],
  machines: Machine[],
  startAt: string,
  endAt: string,
  pollingRate: string,
  pushRate: string
}
