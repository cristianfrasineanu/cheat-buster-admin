import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';

import { IEndpointsConfiguration } from '../../endpoints-configuration.interface';
import { ENDPOINTS_CONFIGURATION } from '../../app-config.constants';
import { Job } from './job.model';
import { Rule } from './rule.model';
import { Machine } from '../machine/machine.model';

@Injectable()
export class JobsService {
  constructor(@Inject(ENDPOINTS_CONFIGURATION) private endpointsConfiguration: IEndpointsConfiguration,
              private http: HttpClient) {
  }

  loadJobs(): Observable<Job[]> {
    return this.http
      .get(this.endpointsConfiguration.LIST_JOBS_ENDPOINT)
      .pipe(map((res: any) => {
        const jobs: Job[] = [];
        for (const rawJob of res) {
          const rules: Rule[] = [];
          for (const rawRule of rawJob.rules) {
            rules.push(<Rule> {
              id: rawRule.id,
              action: rawRule.action,
              type: rawRule.type,
              filter: rawRule.filter,
            });
          }
          const machines: Machine[] = [];
          for (const rawMachine of rawJob.machines) {
            machines.push(<Machine> {
              name: rawMachine.machineName,
            });
          }

          jobs.push(<Job> {
            id: rawJob.id,
            cluster: rawJob.accordingCluster,
            rules,
            machines,
            startAt: rawJob.startAt,
            endAt: rawJob.endAt,
            pollingRate: rawJob.pollingRate,
            pushRate: rawJob.pushRate,
          });
        }

        return jobs;
      }));
  }

  createJob(job: Job): Observable<object> {
    const formattedJob = this.formatJob(job, true);
    return this.http
      .post(this.endpointsConfiguration.CREATE_JOB_ENDPOINT, formattedJob);
  }

  deleteJob(jobId: number): Observable<object> {
    return this.http
      .delete(this.endpointsConfiguration.DELETE_JOB_ENDPOINT(jobId));
  }

  updateJob(job: Job): Observable<object> {
    const formattedJob = this.formatJob(job, false);
    return this.http
      .put(this.endpointsConfiguration.UPDATE_JOB_ENDPOINT(job.id), formattedJob);
  }

  private formatJob(job: Job, createFlag: boolean) {
    const formattedJob = {
      id: job.id,
      accordingCluster: job.cluster,
      startAt: job.startAt,
      endAt: job.endAt,
      pollingRate: job.pollingRate,
      pushRate: job.pushRate,
      machines: [],
      rules: [],
    };
    createFlag && delete formattedJob.id && delete formattedJob.machines && delete formattedJob.rules;

    if (!createFlag) {
      for (const machine of job.machines) {
        formattedJob.machines.push({ machineName: machine.name });
      }
      for (const rule of job.rules) {
        const formattedRule = {
          id: rule.id,
          jobId: formattedJob.id,
          action: rule.action,
          type: rule.type,
          filter: rule.filter,
        };
        formattedJob.rules.push(formattedRule);
      }
    }

    return formattedJob;
  }
}
