export interface Rule {
  id: number,
  action: number,
  type: number,
  filter: string
}
