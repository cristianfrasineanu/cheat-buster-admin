export interface MachineEvent {
  type: string,
  title: string,
  description: string,
}
