import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';

import { Machine } from './machine.model';
import { NbAuthToken, NbTokenService } from '@nebular/auth';
import { MachineEvent } from './machine-event';
import { IEndpointsConfiguration } from '../../endpoints-configuration.interface';
import { ENDPOINTS_CONFIGURATION } from '../../app-config.constants';
import { map } from 'rxjs/operators';
import { Screenshot } from './screenshot.model';

@Injectable()
export class MachinesService {
  public readonly toastsSource$: Subject<MachineEvent> = new Subject();

  private connectionUrl: string;
  private adminSocket$: WebSocketSubject<string>;

  private _machines: BehaviorSubject<Machine[]> = new BehaviorSubject([]);
  public readonly machines$: Observable<Machine[]> = this._machines.asObservable();

  constructor(private tokenService: NbTokenService,
              @Inject(ENDPOINTS_CONFIGURATION) private endpointsConfiguration: IEndpointsConfiguration,
              private http: HttpClient) {
    this.connectionUrl = this.endpointsConfiguration.MACHINE_STATUS_ENDPOINT;
  }

  openConnection(): void {
    this.tokenService.get().subscribe((token: NbAuthToken) => {
      this.adminSocket$ = WebSocketSubject.create(`${this.connectionUrl}${token.getValue()}`);
      this.adminSocket$.subscribe(
        this.handleServerMessage.bind(this),
        this.handleSocketError.bind(this),
        this.handleSuccessfulClose.bind(this),
      )
    });
  }

  closeConnection(): void {
    this.adminSocket$.unsubscribe();
    this._machines.next([]);
  }

  private handleServerMessage(message: Message): void {
    const parsedMachines: Machine[] = message.action !== '_info' ? this.parsePayload(message.data) : [];
    switch (message.action) {
      case '_info':
        console.info(message.data);
        break;
      case '_add':
        this.handleAdd(parsedMachines);
        break;
      case '_update':
        this.handleUpdate(parsedMachines);
        break;
      case '_delete':
        this.handleDelete(parsedMachines);
        break;
      default:
        console.error('Unexpected message action from the server.');
    }
  }

  private handleAdd(parsedMachines: Machine[]) {
    const newMachines = this._machines.getValue().concat(parsedMachines);
    this._machines.next(newMachines);
    parsedMachines.forEach((newMachine) => {
      this.toastsSource$.next({
        type: 'success',
        title: 'New machine',
        description: `${newMachine.name} connected successfuly.`,
      });
    });
  }

  private handleUpdate(parsedMachines: Machine[]) {
    for (const parsed of parsedMachines) {
      this.updateMachine(parsed.name, parsed);
    }
  }

  private updateMachine(targetName: string, newStatus: Machine): void {
    const currentMachines: Machine[] = this._machines.getValue();
    const targetMachine = currentMachines.find((machine: Machine) => machine.name === targetName);
    if (!targetMachine || _.isEqual(targetMachine, newStatus)) return;

    let updateType: string;
    if (targetMachine.isActive !== newStatus.isActive) updateType = newStatus.isActive ? 'activated' : 'deactivated';
    if (targetMachine.isLocked !== newStatus.isLocked) updateType = newStatus.isLocked ? 'locked' : 'unlocked';

    Object.assign(targetMachine, newStatus);
    this._machines.next(currentMachines);

    this.toastsSource$.next({
      type: 'info',
      title: 'Status changed',
      description: `${targetMachine.name} was ${updateType}.`,
    })
  }

  private handleDelete(parsedMachines: Machine[]) {
    const currentMachines: Machine[] = this._machines.getValue();

    for (const parsed of parsedMachines) {
      const machineName: string = parsed.name;
      const indexToDelete = currentMachines.findIndex((machine) => machine.name === parsed.name);
      (indexToDelete > -1)
      && currentMachines.splice(indexToDelete, 1)
      && this.toastsSource$.next({
        type: 'warning',
        title: 'Disconnected machine',
        description: `${machineName} has disconnected.`,
      });
    }
    this._machines.next(currentMachines);
  }

  private handleSocketError(error): void {
    this.toastsSource$.next({
      type: 'error',
      title: 'Connection error',
      description: 'An unexpected error has occured.',
    });
    console.error(error);
  }

  private handleSuccessfulClose(): void {
    console.warn('Socket connection closed successfully.');
  }

  private parsePayload(payload: any): Machine[] {
    const rawPayload: any = [].concat(payload);
    const parsed: Machine[] = [];
    for (const raw of rawPayload) {
      parsed.push({
        name: raw.machineName,
        isActive: raw.isActive,
        isLocked: raw.isLocked,
        jobCluster: raw.jobCluster,
      });
    }

    return parsed;
  }

  public lock(machineName: string) {
    this.adminSocket$.next(JSON.stringify({ action: '_lock', data: machineName }));
  }

  public unlock(machineName: string) {
    this.adminSocket$.next(JSON.stringify({ action: '_unlock', data: machineName }));
  }

  public terminateConnection(machineName: string) {
    this.adminSocket$.next(JSON.stringify({ action: '_terminate', data: machineName }));
  }

  public loadMachineNames(): Observable<any> {
    return this.http.get(this.endpointsConfiguration.MACHINE_NAMES_ENDPOINT);
  }

  public loadScreenshots(machineName: string): Observable<Screenshot[]> {
    return this.http.get(this.endpointsConfiguration.SCREENSHOTS_ENDPOINT, {
      responseType: 'json',
      params: { machineName },
    }).pipe(map((res: any) => {
      const screenshots: Screenshot[] = [];
      for (const rawScreenshot of res) {
        screenshots.push(<Screenshot> {
          fileName: rawScreenshot.fileName,
          data: rawScreenshot.data,
          mimeType: rawScreenshot.mimeType,
        });
      }
      return screenshots;
    }));
  }
}

interface Message {
  action: string;
  data: Machine | Machine[]
}
