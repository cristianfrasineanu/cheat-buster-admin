export interface Machine {
  name: string,
  isActive: boolean,
  isLocked: boolean,
  jobCluster: string
}
