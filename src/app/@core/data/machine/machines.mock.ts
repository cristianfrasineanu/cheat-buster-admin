import { Machine } from './machine.model';

export const machineMocks = <Machine[]> [
  {
    name: 'FOO01',
    isActive: false,
    isLocked: false,
    jobCluster: 'Default',
  },
  {
    name: 'FOO02',
    isActive: false,
    isLocked: false,
    jobCluster: 'Default',
  },
  {
    name: 'FOO03',
    isActive: true,
    isLocked: false,
    jobCluster: 'Alpha',
  },
  {
    name: 'FOO04',
    isActive: true,
    isLocked: false,
    jobCluster: 'Beta',
  },
  {
    name: 'FOO05',
    isActive: false,
    isLocked: false,
    jobCluster: 'Delta',
  },
  {
    name: 'FOO06',
    isActive: false,
    isLocked: false,
    jobCluster: 'Default',
  },
];
