export interface Screenshot {
  fileName: string,
  // The base64 encoded string representing the image.
  data: string,
  mimeType: string,
}
