import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';

import { MachinesService } from './machines.service';
import { Machine } from './machine.model';

export class MachineDataSource extends DataSource<any> {
  constructor(private machineService: MachinesService) {
    super();
  }

  connect(): Observable<Machine[]> {
    return this.machineService.machines$;
  }

  disconnect() {
  }
}
