import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbAuthModule, NbEmailPassAuthProvider } from '@nebular/auth';
import { NbSecurityModule, NbRoleProvider } from '@nebular/security';
import { of as observableOf } from 'rxjs/observable/of';

import { throwIfAlreadyLoaded } from './module-import-guard';
import { DataModule } from './data/data.module';
import { AnalyticsService } from './utils/analytics.service';
import { ENDPOINTS_CONFIGURATION, endpointsConfiguration, timezone, TIMEZONE_VALUE } from './app-config.constants';

const socialLinks = [];

const NB_CORE_PROVIDERS = [
  ...DataModule.forRoot().providers,
  ...NbAuthModule.forRoot({
    providers: {
      email: {
        service: NbEmailPassAuthProvider,
        config: {
          baseEndpoint: '',
          token: {
            key: 'result',
          },
          login: {
            rememberMe: false,
            endpoint: '/api/authenticate',
            method: 'post',
            defaultErrors: ['Unexpected authentication error.'],
          },
          errors: {
            key: 'errors',
          },
        },
      },
    },
    forms: {
      login: {
        socialLinks,
        rememberMe: false,
      },
      validation: {
        password: {
          minLength: 7,
        },
      },
    },
  }).providers,
  NbSecurityModule.forRoot({
    accessControl: {
      guest: {
        view: '*',
      },
      user: {
        parent: 'guest',
        create: '*',
        edit: '*',
        remove: '*',
      },
    },
  }).providers,
  {
    provide: NbRoleProvider,
    useValue: {
      getRole: () => {
        return observableOf('guest'); // here you could provide any role based on any auth flow
      },
    },
  },
  AnalyticsService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    NbAuthModule,
  ],
  declarations: [],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,
      providers: [
        ...NB_CORE_PROVIDERS,
        {
          provide: ENDPOINTS_CONFIGURATION,
          useValue: endpointsConfiguration,
        },
        {
          provide: TIMEZONE_VALUE,
          useValue: timezone,
        },
      ],
    };
  }
}
