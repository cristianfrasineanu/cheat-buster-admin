export interface IEndpointsConfiguration {
  MACHINE_STATUS_ENDPOINT: string,
  MACHINE_NAMES_ENDPOINT: string,
  SCREENSHOTS_ENDPOINT: string,
  LIST_JOBS_ENDPOINT: string,
  CREATE_JOB_ENDPOINT: string,
  DELETE_JOB_ENDPOINT: (id: number) => string,
  UPDATE_JOB_ENDPOINT: (id: number) => string
}
